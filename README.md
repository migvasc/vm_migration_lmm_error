# Error in the LMM while migrating a VM

## Scenario:

Using the cluster_multi.xml platform from the examples and considering two homogeneous physical machines (PMs). Those PMs have a speed of 1 gigaflop/s and energy consumption of 1 J/s when using 1 core and 0 J/s when idle.

When I compute 3Gf into a PM without migration, it takes 3 seconds and consumes 3J. 

The VM is created in PM4 and starts computing 3Gf. After 1 second, the VM is migrated to the PM2.

I'm getting the wrong energy consumption when I use the sleep_for in the async execution method. I was using the sleep_for inside a while loop that uses the test() method in order to validate whether the execution has finished.

When I execute showing the debug log, I got an error regarding the LMM for the constraints, and the execution is interrupted:

````
[1.973393] simgrid/src/surf/surf_c_bindings.cpp:66: [surf_kernel/DEBUG] Looking for next event in virtual models
[1.973393] simgrid/src/plugins/vm/VirtualMachineImpl.cpp:146: [surf_vm/DEBUG] assign 0.000000 to vm VM0 @ pm node-0.4cores.org
[1.973393] simgrid/src/kernel/resource/Model.cpp:42: [resource/DEBUG] Before share resources, the size of modified actions set is 0
[1.973393] simgrid/src/kernel/lmm/maxmin.cpp:495: [surf_maxmin/DEBUG] Active constraints : 1
[1.973393] simgrid/src/kernel/lmm/maxmin.cpp:437: [surf_maxmin/DEBUG] MAX-MIN ( '3'(1.000000) '1982'(0.000000) )
[1.973393] simgrid/src/kernel/lmm/maxmin.cpp:440: [surf_maxmin/DEBUG] Constraints
[1.973393] simgrid/src/kernel/lmm/maxmin.cpp:456: [surf_maxmin/DEBUG] 	(1.000000.'3'(1000000000.000000) + 1.000000.'1982'(0.000000) + 0) <= 0.000000 ('69')
[1.973393] simgrid/src/kernel/lmm/maxmin.cpp:459: [root/CRITICAL] Incorrect value (1000000000.000000 is not smaller than 0.000000): 1e+09
````

From what I've seen in the log, this error occurs after the third step of the migration.

To execute the C++  version, it is first necessary to compile the project, and then run it. Example:

Compile
```
g++ example.cpp -o app /usr/local/lib/libsimgrid.so.3.25 -I /opt/simgrid/include
```

Run without logs:
```
./app cluster_multi.xml deployment.xml
```

Run displaying the logs:
```
./app cluster_multi.xml deployment.xml --log=root.:debug
```
## Description of the deployment file:

The deployment file ```` deployment.xml ```` accepts 4 arguments:
1. The amount of flops to be computed
2. Value of time used for the sleep_for inside the loop. The lower the value, more frequently it will be checked whether the execution has finished
3. Execution type
    - 0 = Considers test() in a loop to validate if the execution has finished and sleep_for inside the loop to control how frequently the validation will be performed
    - 1 = Executes the test() 10 times
    - 2 = Executes sleep_for using 3s (estimated computation time)
    - 3 = Executes both the test() 10 times and sleep_for using 3s (estimated computation time)
    - 4 = Executes the computation without the test() and sleep_for
4. Whether the VM will be migrated (1) or not (0)
