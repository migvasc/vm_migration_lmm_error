/* Copyright (c) 2007-2020. The SimGrid Team.
 * All rights reserved.                                                     */

/* This program is free software; you can redistribute it and/or modify it
 * under the terms of the license (GNU LGPL) which comes with this package. */
#include "simgrid/s4u.hpp"
#include "simgrid/plugins/energy.h"
#include "simgrid/s4u/VirtualMachine.hpp"
#include <cmath>
#include "simgrid/plugins/live_migration.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(example, "Messages specific for this example");

static void printEnergyConsumption(){
  XBT_INFO("printEnergyConsumption");
  for (simgrid::s4u::Host *pm : simgrid::s4u::Engine::get_instance()->get_all_hosts() ){
    if(!dynamic_cast<simgrid::s4u::VirtualMachine *>(pm)){      
      XBT_INFO("HOST %s, energy consumed %f",pm->get_cname(),sg_host_get_consumed_energy(pm));
      XBT_INFO("current consumption  %f",sg_host_get_current_consumption(pm));               
    }      
  }
}

class TaskExecutor{

private:
  double flopAmount;
  int execType;
  double sleepAmount;

public:
    explicit TaskExecutor() = default;
    explicit TaskExecutor(double computeAmount, double sleep, int exec_type)
    {
        flopAmount = computeAmount;
        execType = exec_type;
        sleepAmount = sleep;
    }

    void operator()(){          
          double begin = simgrid::s4u::Engine::get_clock();                             
          sg_host_energy_update_all();
          switch (execType)
          {
          case 0:
            /* with sleep for and test in a loop */
            compute_case0();
            break;
          case 1:
            /* only with test */
            compute_case1();
            break;
            case 2:
            /* only sleep */
            compute_case2();
            break;
          case 3:
            /* test and sleep */
            compute_case3();
            break;
          case 4:
            /* no test nor sleep for */
            compute_case4();
            break;
          default:
            break;
          }
                    
          
          sg_host_energy_update_all();
          double end = simgrid::s4u::Engine::get_clock();          
          XBT_INFO("Task executed time: %f", end - begin);     
          sendFinishMessage();
    }



    void compute_case0()
    {                    
          simgrid::s4u::ExecPtr ptr =  simgrid::s4u::this_actor::exec_init(flopAmount);                            
          ptr->start();                                                  
          while (not ptr->test()){
            simgrid::s4u::this_actor::sleep_for(sleepAmount);
          }
          ptr->wait();          
    }

    void compute_case1()
    {          
          simgrid::s4u::ExecPtr ptr =  simgrid::s4u::this_actor::exec_init(flopAmount);                            
          ptr->start();                          
          
          ptr->test();                        
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();                    
          ptr->wait();          


    }

    void compute_case2()
    {          
          simgrid::s4u::ExecPtr ptr =  simgrid::s4u::this_actor::exec_init(flopAmount);                            
          ptr->start();                                                  
          simgrid::s4u::this_actor::sleep_for(3);
          ptr->wait();          
    }

    void compute_case3()
    {          
          simgrid::s4u::ExecPtr ptr =  simgrid::s4u::this_actor::exec_init(flopAmount);                            
          ptr->start();                   
          ptr->test();                        
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();
          ptr->test();                    
          simgrid::s4u::this_actor::sleep_for(3);                                                   
          ptr->wait();          
    }

    void compute_case4()
    {          
          simgrid::s4u::ExecPtr ptr =  simgrid::s4u::this_actor::exec_init(flopAmount);                            
          ptr->start();                                                            
          ptr->wait();          
    }

    void sendFinishMessage(){        
          simgrid::s4u::Mailbox* father = simgrid::s4u::Mailbox::by_name("mb_executor_"+simgrid::s4u::this_actor::get_host()->get_name());
          int data =1;
          father->put_init(&data,0)->detach();
    }

};

class VMExecutor{

private:
        simgrid::s4u::VirtualMachine* vm;
        double flopsAmount;
        double sleepAmount;    
        int execType;

public:

        explicit VMExecutor() = default;  

        explicit VMExecutor(simgrid::s4u::VirtualMachine* avm, 
                            double flops, 
                            double sleep,
                            int exec)
        {
            vm=avm;
            flopsAmount = flops;            
            sleepAmount = sleep;
            execType=exec;
            

        }

        void operator()()
        {
            simgrid::s4u::Mailbox* myMailbox = simgrid::s4u::Mailbox::by_name("mb_executor_" + vm->get_name());
            myMailbox->set_receiver(simgrid::s4u::Actor::self());        
            std::vector<simgrid::s4u::ActorPtr> actors;
            std::vector<TaskExecutor> execs;
            TaskExecutor exec;       
            for(int i=0; i<vm->get_core_count();i++)
            {
              exec = TaskExecutor(flopsAmount,sleepAmount,execType);
              execs.push_back(exec);
              actors.push_back(simgrid::s4u::Actor::create(vm->get_name()+"_executor"+std::to_string(i), vm, std::ref(exec))); 
            }

           int tasksFinished =vm->get_core_count();           
           void * data;

           while(tasksFinished > 0)
            {                             
              data = myMailbox->get();                                   
              tasksFinished--;           
            }            

            XBT_INFO("ALL TASKS FINISHED");
            for(int i=0; i<vm->get_core_count();i++)
            {              
              actors[i]->kill();
            }
            sg_host_energy_update_all();
            vm->destroy();
            simgrid::s4u::this_actor::exit();          
        }
};

class MainActor
{

private:
    double flopsAmount;
    double sleepAmount;    
    int execType;
    bool willMigrate;

public:

    static void vm_migrate(simgrid::s4u::VirtualMachine* vm, simgrid::s4u::Host* dst_pm)
    {
        XBT_INFO("%s migration started! ", vm->get_cname());
        const simgrid::s4u::Host* src_pm = vm->get_pm();  
        double mig_sta             = simgrid::s4u::Engine::get_clock();  
        sg_host_energy_update_all();
        printEnergyConsumption();
        sg_vm_migrate(vm, dst_pm);
        printEnergyConsumption();
        sg_host_energy_update_all();
        double mig_end = simgrid::s4u::Engine::get_clock();
        XBT_INFO("######## %s migrated: %s->%s in %g s ########", vm->get_cname(), src_pm->get_cname(), dst_pm->get_cname(), mig_end - mig_sta);  
        sg_host_energy_update_all();        
    }

    

    explicit MainActor(std::vector<std::string> args){        
        flopsAmount = std::stod(args[1]);            
        sleepAmount = std::stod(args[2]);
        execType = std::stoi(args[3]);
        willMigrate  = std::stoi(args[4] ) == 1;
    }

    void operator()(){        
        simgrid::s4u::Host *pm2 = simgrid::s4u::Host::by_name("node-0.2cores.org");
        simgrid::s4u::Host *pm4 = simgrid::s4u::Host::by_name("node-0.4cores.org");
        simgrid::s4u::VirtualMachine *vm0;
        int nCores = 1;
        vm0 = new simgrid::s4u::VirtualMachine("VM0", pm4, nCores);
        vm0->set_ramsize(110000000);
        vm0->start();            
        simgrid::s4u::Actor::create("killer", pm4, VMExecutor(vm0,flopsAmount,sleepAmount,execType));          
        if(willMigrate){
            XBT_INFO("VM will be migrated...");
            simgrid::s4u::this_actor::sleep_for(1);
            simgrid::s4u::Actor::create("mig_wrk", pm4, vm_migrate, vm0, pm2);        
        }

        sg_host_energy_update_all();
    }

};


int main(int argc, char* argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  sg_vm_live_migration_plugin_init();
  sg_host_energy_plugin_init();
  e.register_actor<MainActor>("mainactor");/* Submits VMs */  
  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);
  e.run(); 
  XBT_INFO("Simulation is over");    
  return 0;
}
